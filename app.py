from flask import Flask, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
import os
import sys
import logging
import requests
from typing import List
import functools

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

class GetRatesForm(FlaskForm):
    city = StringField('City', validators=[DataRequired()])
    checkin = StringField('Check In', validators=[DataRequired()])
    checkout = StringField('Check Out', validators=[DataRequired()])
    submit = SubmitField('Submit')

class RatesProvider():
  def __init__(self, src_base_url: str):
    self.src_base_url = src_base_url

  def __get_request(self, body: dict):
    r = requests.post(url = self.src_base_url, data = body)
    return r.json()

  def convert_hotel_list_to_dict(hotels: List):
    result = dict()
    for hotel in hotels:
      result[hotel["id"]] = hotel
    return result

  @functools.lru_cache(maxsize=None)
  def get_snap_rates(self, city:str, checkin:str, checkout: str):
    app.logger.debug("get snap called")
    response = self.__get_request({
      "city": city,
      "checkin": checkin,
      "checkout": checkout,
      "provider": "snaptravel"
    })
    return RatesProvider.convert_hotel_list_to_dict(response["hotels"])

  @functools.lru_cache(maxsize=None)
  def get_retail_rates(self, city:str, checkin:str, checkout: str):
    app.logger.debug("get retail called")
    response = self.__get_request({
      "city": city,
      "checkin": checkin,
      "checkout": checkout,
      "provider": "retail"
    })
    return RatesProvider.convert_hotel_list_to_dict(response["hotels"])

  def get_common_rates(self,city: str, checkin: str, checkout: str):
    snap_rates = self.get_snap_rates(city, checkin, checkout)
    snap_rates_ids = set(snap_rates.keys())
    retail_rates = self.get_retail_rates(city, checkin, checkout)
    retail_rates_ids = set(retail_rates.keys())
    app.logger.debug(len(snap_rates))
    app.logger.debug(len(retail_rates))
    common_rates_ids = snap_rates_ids.intersection(retail_rates_ids)
    common_hotels = []
    for hotel_id in common_rates_ids:
      common_hotel = {**snap_rates[hotel_id], **retail_rates[hotel_id]}
      common_hotel["snap_price"] =snap_rates[hotel_id]["price"]
      common_hotel["retail_price"] =retail_rates[hotel_id]["price"]
      del common_hotel["price"]
      common_hotels.append(common_hotel)
    app.logger.debug(common_hotels)
    app.logger.debug(len(common_hotels))
    return common_hotels

rates_provider: RatesProvider = RatesProvider("https://experimentation.snaptravel.com/interview/hotels")


@app.route('/')
def main_page():
    return redirect("/rates/form", code=302)

@app.route('/rates/form', methods=['GET', 'POST'])
def rates_form():
    form = GetRatesForm()
    if form.validate_on_submit():
      app.logger.info(f"Rates requested for city: {form.city.data}, checkin: {form.checkin.data}, checkout: {form.checkout.data}")
      common_hotels = rates_provider.get_common_rates(form.city.data, form.checkin.data, form.checkout.data)
      return render_template('hotels.html', hotels=common_hotels)
    return render_template('request_form.html', form=form)